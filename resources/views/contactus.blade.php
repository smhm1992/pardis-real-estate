<!DOCTYPE html>
<html>
<head>
    @include('sections.head')
</head>

<body>

<header class="topeleman">
    @include('sections.header')
</header>

<div class="clear"></div>

<main class="maineleman">
{{--    <section class="mapbox" id="myMap">--}}

{{--    </section>--}}



    <section class="mapbox">
        <a href="https://www.google.com/maps/place/36%C2%B041'38.2%22N+52%C2%B052'37.1%22E/@36.6621418,52.8440419,12z/data=!4m5!3m4!1s0x0:0x0!8m2!3d36.69394!4d52.876958" target="_blank"><img src="/assets/images/map.jpg" style="width: 100%; height: 100%;"></a>
    </section>



<div class="clear"></div>
    <section class="container">
        <div class="contentbox">


            <div class="addressbox">
                <div class="addressboxtop">

                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold; font-size: 17px;">اطلاعات تماس</p>
                        </div>
                        <div class="contactcrossleft">

                        </div>
                    </div>

                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">آدرس</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">مازندران، جویبار، منطقه توریستی چپکرود</p>
                        </div>
                    </div>

                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">تلفن</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">7482   4256   11   98+</p>
                        </div>
                    </div>
                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">فکس</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">8003   4256   11   98+</p>
                        </div>
                    </div>
                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">همراه یک</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">5292   326   911   98+</p>
                        </div>
                    </div>
                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">همراه دو</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">5292   626   911   98+</p>
                        </div>
                    </div>
                    <div style="width: 100%; height: 30px; float: right;">
                        <div class="contactcrossright">
                            <p class="p" style="font-weight: bold;">ایمیل</p>
                        </div>
                        <div class="contactcrossleft">
                            <p class="p">info@pardis-shomal.com</p>
                        </div>
                    </div>



                </div>

                <div class="addressboxbottom">
                    <p class="p" style="font-weight: bold; font-size: 17px;">ما را در صفحات مجازی دنبال کنید..</p><br>
                    <a href="#"><i class="fa fa-telegram fa-2x" style="color:#66a992;"></i></a> &nbsp &nbsp &nbsp
                    <a href="#"><i class="fa fa-instagram fa-2x" style="color:#66a992;"></i></a>
                </div>
            </div>


            <div class="formbox">
                <div class="formboxtop">
                    <p class="p" style="font-weight: bold; font-size: 17px"> ارسال پیام به مدیریت</p><br>
                    <p class="p" style="margin-top: -8px;">جهت ثبت ملک میتوانید از فرم زیر استفاده کنید تا همکاران ما با شما تماس بگیرند.</p>
                    <p class="p" style="color: red">** در نوشتن آدرس ایمیلتان جهت پاسخ ما به شما کمی دقت لازم است **</p>
                </div>
                <form method="post" action="{{ route('ticket.store') }}">
                    {{ csrf_field() }}
                    <input type="text" name="sender_name"  placeholder="نام و نام خانوادگی *">
                    <input type="text" name="sender_phone" placeholder="تلفن *">
                    <input type="email" name="sender_email" placeholder="ایمیل *">
                    <textarea name="message" placeholder="متن..."></textarea>
                    <button>ارسال پیام</button>
                </form>
            </div>
        </div>
    </section>
<div class="clear"></div>

    <div>
        <img src="/assets/images/Bottom.jpg" style="width: 100%; height: 200px; ">
    </div>
</main>

<div class="clear"></div>

<footer class="bottomeleman">
    @include('sections.footer')
</footer>

</body>



{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqt3PdbzxzA8gwGbOLpDIERSdZwNZCrhE&callback=myMap"></script>--}}
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCwUC4Qo618QaoHgf5h4Oc-7ghnwj0AWGQ" async="" defer="defer" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {
        jQuery('.toggle-nav').click(function(e) {
            jQuery(this).toggleClass('active');
            jQuery('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });




    $(document).ready(function() {
        $(document).scroll(function(){
            x = $(document).scrollTop();
            if ( x > 198 ) {
                $(".topbar").addClass("topfix");
            }else{
                $(".topbar").removeClass("topfix"); }
        });
    });



    window.onload=function(){
        var myLocation = new google.maps.LatLng(36.693940, 52.876958), mapOptions = {
            zoom: 11,
            center: myLocation,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }, map = new google.maps.Map(document.getElementById("myMap"), mapOptions), infowindow = new google.maps.InfoWindow({
            content: '<div style="direction:rtl;color:#20895e;text-align:right;font:bold 17px Tahoma;">&nbsp &nbsp &nbsp  &nbspواحد املاک پردیس</div>'
        }), marker = new google.maps.Marker({
            position: myLocation,
            map: map
        });
        infowindow.open(map, marker);
    }
</script>


</html>
