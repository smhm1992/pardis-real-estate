<div class="topbar">

    <div class="menubox">
        <nav class="menu">
            <ul class="active">
                <li>
                    <a href="/">صفحه اصلی
                        <div class="menuicons">
                            <i class="fa fa-windows fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>
{{--                <hr>--}}
{{--                <li><a href="/estates">همه املاک</a></li>--}}
{{--                <hr>--}}
{{--                <li><a href="/#">پروژه ها</a></li>--}}
{{--                <hr>--}}
                <li>
                    <a href="/house">خانه و ویلا
                        <div class="menuicons">
                            <i class="fa fa-home fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>
{{--                <hr>--}}
                <li>
                    <a href="/apartment">آپارتمان
                        <div class="menuicons">
                            <i class="fa fa-building fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>

{{--                <hr>--}}
                <li>
                    <a href="/agriculture">کشاورزی و باغ
                        <div class="menuicons">
                            <i class="fa fa-envira fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="/earth">زمین
                        <div class="menuicons">
                            <i class="fa fa-map fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="/commercial">تجاری و صنعتی
                        <div class="menuicons">
                            <i class="fa fa-wrench fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>


                <li>
                    <a href="/aboutus">درباره ما
                        <div class="menuicons">
                            <i class="fa fa-group fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>
{{--                <hr>--}}


                <li>
                    <a href="/contactus">تماس با ما
                        <div class="menuicons">
                            <i class="fa fa-phone fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="/blog" target="_blank">وبلاگ
                        <div class="menuicons">
                            <i class="fa fa-arrow-circle-o-up fa-2x" style="color:#66a992;"></i>
                        </div>
                    </a>
                </li>

            </ul>
            <a class="toggle-nav" href="#">&#9776;</a>

        </nav>

    </div>
    <div class="logobox">
        <a href="/"><img src="/assets/images/logo.png"></a>
        <p style="font-weight: bold;color: #333333;">فرشــــــــــــــــــــــــــاد زرگــــــــــــــــــــــــــــری</p>
    </div>

</div>



