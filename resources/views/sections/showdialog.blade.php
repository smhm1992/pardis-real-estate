
<div class="btn btn-success btn-lg show-dialogbtn" data-toggle="modal"
     data-target="#myModal">
    <p>درخواست کارشناس</p>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">فرم درخواست کارشناس</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('order.store') }}">
                    {{ csrf_field() }}
                    &nbsp;<label>نام <span style="color:red; ">*</span></label>
                    <input name="FName" class="form-control" placeholder=" " ><br>

                    &nbsp;<label>نام خانوادگی <span style="color:red; ">*</span></label>
                    <input name="LName" class="form-control" placeholder=" " required><br>
                    &nbsp;
                    &nbsp;<label> تلفن<span style="color:red; ">*</span></label>
                    <input name="phone" class="form-control" placeholder="" required><br>
                    &nbsp;
                    &nbsp;<label>کد ملک <span style="color:red; ">*</span></label>
                    <input name="estCode" class="form-control" placeholder="" required><br>

                    &nbsp;<label>توضیحات اضافه<span style="color:red; "></span></label>
                    <textarea class="form-control" name="description" placeholder=""></textarea>

                    <div class="modal-footer">
                        <button class="btn btn-success" >ثبت درخواست</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">
                            انصراف
                        </button>
                        <p style="color: #00ba37; float: right; margin-top: 5px;">بعد از ثبت درخواست، کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
