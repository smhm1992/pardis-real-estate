<div class="footermenubox">
        <p style="font-size: 18px;">دسترسی سریع به منو</p>
    <ul>
        <a href="/"><li><i class="fa fa-windows fa-1x"></i> &nbsp صفحه اصلی</li></a>
        {{--                <hr>--}}
{{--        <a href="/#"><li><i class="fa fa-circle fa-1x"></i> &nbsp همه املاک</li></a>--}}
        {{--                <hr>--}}
{{--        <a href="/#"><li><i class="fa fa-home fa-1x"></i> پروژه ها</li></a>--}}
        {{--                <hr>--}}
        <a href="/house"><li><i class="fa fa-home fa-1x"></i> &nbsp خانه و ویلا</li></a>

        <a href="/apartment"><li><i class="fa fa-building fa-1x"></i> &nbsp آپارتمان</li></a>

        <a href="/agriculture"><li><i class="fa fa-envira fa-1x"></i> &nbsp کشاورزی و باغ</li></a>

        <a href="/earth"><li><i class="fa fa-map fa-1x"></i> &nbsp زمین</li></a>

        <a href="/commercial"><li><i class="fa fa-wrench fa-1x"></i> &nbsp تجاری و صنعتی</li></a>

        {{--                <hr>--}}
        <a href="/aboutus"><li><i class="fa fa-group fa-1x"></i> &nbsp درباره ما</li></a>
        {{--                <hr>--}}
        <a href="/contactus"><li><i class="fa fa-phone fa-1x"></i> &nbsp تماس با ما</li></a>

        <a href="/blog"><li><i class="fa fa-arrow-circle-o-up fa-1x"></i> &nbsp وبلاگ</li></a>

    </ul>
</div>




<div class="footercontactusbox">
    <p style="font-size: 18px;">تماس با ما</p>

    <ul>
        <li><i class="fa fa-phone fa-1x"></i>&nbsp  تلفن: 7482 &nbsp 4256 &nbsp 11 &nbsp 98+ </li>
        <li><i class="fa fa-paragraph fa-1x"></i>&nbsp  فکس: 8003 &nbsp 4256 &nbsp 11 &nbsp 98+</li>
        <li><i class="fa fa-mobile-phone fa-1x"></i>&nbsp  همراه یک: 5292 &nbsp 326 &nbsp 911 &nbsp 98+ </li>
        <li><i class="fa fa-mobile fa-1x"></i>&nbsp  همراه دو: 5292 &nbsp 626 &nbsp 911 &nbsp 98+ </li>
        <li><i class="fa fa-mail-reply-all fa-1x"></i>  ایمیل: info@pardis-shomal.com</li>
    </ul>


{{--        <a href="#"><i class="fa fa-facebook fa-2x" style="color:#66a992;"></i></a>--}}
{{--        <a href="#"><i class="fa fa-twitter fa-2x" style="color:#66a992;"></i></a>--}}
        <a href="#"><i class="fa fa-telegram fa-2x" style="color:#66a992;"></i></a>
        <a href="#"><i class="fa fa-instagram fa-2x" style="color:#66a992;"></i></a>


</div>




<div class="footeraboutusbox">
    <p style="font-size: 18px;">توضیح مختصر</p>
    <p class="p" style="padding: 0px 0px 0px 0px !important;">
        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد تا با نرم‌افزارها شناخت بیشتری را برای طراحان رایانه ای علی‌الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
    </p>
</div>
