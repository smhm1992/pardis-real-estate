<!DOCTYPE html>
<html>
<head>
    @include('sections.head')
</head>

<body>

<header class="topeleman">
    @include('sections.header')
</header>

<div class="clear"></div>

<main class="maineleman">
    <section class="mapbox">
        <img src="/assets/images/service-pic.jpg" style="width: 100%; height: 100%;">
        <div class="namebox"><br><center>
            <p style="font-weight: bold; font-size: 24px;color: #FDFDFD;">علی (فرشاد) زرگری</p><br>
                <p style="font-weight: bold; font-size: 20px;color: #FDFDFD;">مدیریت واحد املاک پردیس</p></center>
        </div>
    </section>
    <div class="clear"></div>
    <section class="container" style="margin-top: 50px;">

        <div class="propertiesbox" style="padding: 50px;">
            <p class="p" style="font-weight: bold; font-size: 16px">اهداف ما</p><br>
            <p class="p">
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد تا با نرم‌افزارها شناخت بیشتری را برای طراحان رایانه ای علی‌الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می‌توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساساً مورد استفاده قرار گیرد.
            </p>
        </div>


        <div class="sliderbox" style="padding: 50px;">
            <p class="p" style="font-weight: bold; font-size: 16px;">درباره ما</p><br>
            <p class="p">
            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد تا با نرم‌افزارها شناخت بیشتری را برای طراحان رایانه ای علی‌الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می‌توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساساً مورد استفاده قرار گیرد.
            </p>
        </div>



    </section>
    <div class="clear"></div>

    <div>
        <img src="/assets/images/Bottom.jpg" style="width: 100%; height: 200px; ">
    </div>
</main>

<div class="clear"></div>

<footer class="bottomeleman">
    @include('sections.footer')
</footer>

</body>



{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqt3PdbzxzA8gwGbOLpDIERSdZwNZCrhE&callback=myMap"></script>--}}
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCwUC4Qo618QaoHgf5h4Oc-7ghnwj0AWGQ" async="" defer="defer" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {
        jQuery('.toggle-nav').click(function(e) {
            jQuery(this).toggleClass('active');
            jQuery('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });




    $(document).ready(function() {
        $(document).scroll(function(){
            x = $(document).scrollTop();
            if ( x > 198 ) {
                $(".topbar").addClass("topfix");
            }else{
                $(".topbar").removeClass("topfix"); }
        });
    });


</script>


</html>
