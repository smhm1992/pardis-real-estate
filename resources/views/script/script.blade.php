@push('main-script)
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/fastclick.js')}}"></script>
    <script src="{{asset('assets/js/nprogress.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{asset('assets/js/icheck.min.js')}}"></script>
    <script src="{{asset('assets/js/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/js/Chart.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('assets/js/gauge.min.js')}}"></script>
    <script src="{{asset('assets/js/skycons.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.time.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('assets/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('assets/js/curvedLines.js')}}"></script>
    <script src="{{asset('assets/js/date.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.vmap.js')}}"></script>
    <script src="{{asset('assets/js/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('assets/js/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>

@endpush