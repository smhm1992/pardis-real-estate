@extends('layout.panel.master')
@section('content')
    <div class="right_col" role="main">
        <div class="col-md-12">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                              data-toggle="tab" aria-expanded="true">پرسنلی</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                        data-toggle="tab" aria-expanded="false">ویرایش مشخصات</a>
                    </li>
                    {{--<li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2"--}}
                    {{--data-toggle="tab" aria-expanded="false">ویرایش مشخصات</a>--}}
                    {{--</li>--}}
                </ul>
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                {{--<div class="x_title">--}}
                                {{--<h2>جدول راه‌راه--}}
                                {{--<small>زیر عنوان جدول راه‌راه</small>--}}
                                {{--</h2>--}}
                                {{--<ul class="nav navbar-right panel_toolbox">--}}
                                {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                {{--</li>--}}
                                {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                {{--</li>--}}
                                {{--</ul>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                                <div class="x_content">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>نام</th>
                                            <th>نام خانوادگی</th>
                                            <th>ایمیل</th>
                                            <th>شماره تماس</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$admin->first_name}} </td>
                                            <td>{{$admin->last_name}}</td>
                                            <td>{{$admin->email}}</td>
                                            <td>{{$admin->phone_number}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">

                                <div class="x_content">

                                    <form method="POST" action="/panel/edit_profile/{{$admin->id}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col">
                                                <label>نام:</label>
                                                <input type="text" name="first_name"
                                                       class="form-control" placeholder="نام کوچک خود را وارد کنید" value="{{$admin->first_name}}">
                                            </div>
                                            <div class="col">
                                                <label>نام خانوادگی</label>
                                                <input type="text" name="last_name"
                                                       class="form-control" placeholder="نام خانوادگی خود را وارد کنید" value="{{$admin->last_name}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label>پست الکترونیک</label>
                                                <input type="email" name="email"
                                                       class="form-control" placeholder="ایمیل خود را وارد کنید" value="{{$admin->email}}" required>
                                            </div>
                                            <div class="col">
                                                <label>شماره تماس</label>
                                                <input type="text" name="phone_number"
                                                       class="form-control" placeholder="شماره تماس خود را وارد کنید" value="{{$admin->phone_number}}">
                                            </div>
                                        </div>
                                        <br/>
                                        <button class="btn btn-primary" type="submit">ثبت</button>
                                    </form>

                                    <button type="button" style="float: left;" class="btn btn-info btn-lg" data-toggle="modal"
                                            data-target="#myModal">
                                        تغییر کلمه عبور
                                    </button>


                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog"  role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">کلمه عبور جدیدتان را وارد کنید</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="/panel/change_pass/{{$admin->id}}"
                                                oninput='confirm_pass.setCustomValidity(confirm_pass.value != new_pass.value ? "لطفا مقادیر یکسان وارد کنید." : "")'>
                                           @csrf
                                            <div class="row">
                                                <div class="col">
                                                    <input type="password" name="old_pass"
                                                           class="form-control" placeholder="پسورد پیشین" required>
                                                </div>
                                                <div class="col">
                                                    <input type="password" name="new_pass"
                                                           id="password1"     class="form-control" placeholder="پسورد جدید" required>
                                                    <input type="password" name="confirm_pass"
                                                           id="password2"    class="form-control" placeholder="تایپ مجدد" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    انصراف
                                                </button>
                                                <button type="submit" class="btn btn-primary">ثبت</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    {{--<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">--}}
                    {{--<p>چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای--}}
                    {{--شرایط--}}
                    {{--فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می--}}
                    {{--باشد.--}}
                    {{--در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و--}}
                    {{--شرایط--}}
                    {{--سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و--}}
                    {{--جوابگوی--}}
                    {{--سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>

    </div>


@endsection
