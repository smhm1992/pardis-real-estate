@extends('layout.panel.master')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>ثبت منطقه جدید
                    </h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="x_content">

                    <form id="form" method="post" action="{{route('panel.location.store')}}"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="content-group">
                            <input type="text" name="name" placeholder="نام منطقه را وارد کنید" class="form-control">
                            <br>
                            <button type="submit" class="btn btn-success form-control">ثبت
                            </button>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection





