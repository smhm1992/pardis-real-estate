@extends('layout.panel.master')
@section('content')
    <style type="text/css">
        /* DivTable.com */
        .divTable {
            display: table;
            width: 100%;
        }

        .divTableRow {
            display: table-row;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
        }

        .divTableCell, .divTableHead {
            border: 0.5px solid #999999;
            display: table-cell;
            padding: 3px 10px;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
            font-weight: bold;
        }

        .divTableFoot {
            background-color: #EEE;
            display: table-footer-group;
            font-weight: bold;
        }

        .divTableBody {
            display: table-row-group;
        }
    </style>
    <style>


        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            /*content: "\f067";*/
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            /*content: "\f00c";*/
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: absolute;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>
    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>درخواست های کارشناسی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                        <tr>
                            <th>ردیف</th>
                            <th>نام و نام خانوادگی</th>
                            <th>شماره تماس</th>
                            <th>کد ملک درخواستی</th>
                            <th>توضیحات درخواستی</th>
                            <th>وضعیت</th>
                            <th>حذف مورد</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr title="{{$order->description}}">
                                <td>
                                    {{$i++}}
                                </td>
                                <td>
                                    {{$order->FName}} {{$order->LName}}
                                </td>

                                <td>
                                    {{$order->phone}}

                                </td>
                                <td>
                                   {{$order->estCode}}
                                </td>
                                <td>
                                    @if($order->description == "")
                                        <p style="color: blue;">--بدون توضیحات--</p>
                                    @else
{{--                                        {{ Str::limit($order->description, 20 , $end='...') }}--}}
                                        <button type="button" style="width: 100%" class="col-lg-8 btn btn-success"
                                                data-toggle="modal"
                                                data-target="#show_desc{{$order->id}}">
                                            نمایش توضیحات
                                        </button>
                                    @endif
                                </td>

                                <td>
                                    @if($order->orderStatus == 1)
                                        <p style="color: green;">انجام شده</p>
                                    @else
                                        <button type="button" style="width: 100%" class="col-lg-8 btn btn-info"
                                                data-toggle="modal"
                                                data-target="#show_manage{{$order->id}}">
                                            مدیریت
                                        </button>
                                    @endif
                                </td>

                                <td>
                                    <form action="/panel/order/delete_order/{{$order->id}}" method="post">
                                        @csrf
                                        {{--<input type="text" name="order_id" value="{{$order->id}}" style="display: none;"/>--}}

                                        <button type="submit" onclick="return confirm('آیا از این عمل مطمئن هستید؟')" style="width: 100%" class="col-lg-8 btn btn-danger btn-lg">
                                            حذف
                                        </button>

                                    </form>


                                </td>

                            </tr>



                            <div class="modal fade" id="show_desc{{$order->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">مشاهده توضیحات سفارش</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                          توضیحات:   <p>

                                                {{$order->description}}
                                            </p> <br/>


                                        </div>
                                    </div>
                                </div>


                            </div>





                            <div class="modal fade" id="show_manage{{$order->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">تغییر وضعیت</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/panel/order/edit/{{$order->id}}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <select class="form-control js-example-basic-single" name="orderStatus" required>
                                                    <option value="1">انجام شده</option>
                                                </select>

                                                <button type="submit" style="margin-top: 20%; margin-right: 5%;"
                                                        class="btn btn-primary">ثبت
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{$orders->links()}}

            </div>
        </div>


    </div>

@endsection
