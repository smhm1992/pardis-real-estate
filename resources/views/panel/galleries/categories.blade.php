@extends('layout.panel.master')
@section('content')
    <style type="text/css">
        /* DivTable.com */
        .divTable {
            display: table;
            width: 100%;
        }

        .divTableRow {
            display: table-row;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
        }

        .divTableCell, .divTableHead {
            border: 0.5px solid #999999;
            display: table-cell;
            padding: 3px 10px;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
            font-weight: bold;
        }

        .divTableFoot {
            background-color: #EEE;
            display: table-footer-group;
            font-weight: bold;
        }

        .divTableBody {
            display: table-row-group;
        }
    </style>
    <style>


        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            /*content: "\f067";*/
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            /*content: "\f00c";*/
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: absolute;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>
    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>مدیریت دسته بندی های گالری
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                        <tr>
                            <th>ردیف</th>
                            <th>نام</th>
                            <th>توضیحات</th>
                            <th>مشاهده تصویر شاخص</th>
                            <th>مدیریت</th>
                            <th>ورود به گالری</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $cat)

                            <tr>
                                <td>
                                    {{$i++}}
                                </td>
                                <td>
                                    {{$cat->name}}
                                </td>
                                <td>
                                    {{$cat->desc}}
                                </td>
                                <td>
                                    <button type="button" style="" class="col-lg-6 btn btn-success btn-lg"
                                            data-toggle="modal"
                                            data-target="#show_pic{{$cat->id}}">
                                        مشاهده
                                    </button>
                                </td>
                                <td>
                                    <button type="button" style="" class="col-lg-6 btn btn-info btn-lg"
                                            data-toggle="modal"
                                            data-target="#myModal{{$cat->id}}">
                                        مدیریت
                                    </button>
                                </td>
                                <td>
                                    <a href="/panel/gallery/manage_gallery/{{$cat->id}}">ورود</a>
                                </td>
                            </tr>

                            <div class="modal fade" id="show_pic{{$cat->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">تصویر شاخص گالری</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                <div class="text-center">
                                                    <img src="/images/{{$cat->image}}"
                                                         class="img-thumbnail rounded ">
                                                </div>
                                                <br/>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div style="" class="modal fade" id="myModal{{$cat->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">مدیریت دسته بندی</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="/panel/gallery/edit_cat/{{$cat->id}}"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="">نام</label>
                                                        <input name="name" type="text" class="form-control" id=""
                                                               value="{{$cat->name}}"
                                                               placeholder="نام دسته بندی را وارد کنید" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="">توضیحات</label>
                                                        <textarea name="desc" type="text" class="form-control"
                                                                  placeholder="توضیخات را وارد کنید" required>
                                                            {{$cat->desc}}
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-2 control-label text-semibold">آپلود
                                                            تصویر:</label>
                                                        <br/>
                                                        <div class="col-lg-10">
                                                            <input type="file" name="image" class="file-input">
                                                        </div>


                                                    </div>
                                                    <div class="form-group col-md-6">
                                                    </div>
                                                </div>

                                                <button type="submit" style="margin-top: 20%; margin-left: 5%;"
                                                        class="btn btn-primary">ثبت تغییرات
                                                </button>
                                            </form>
                                            <a href="/panel/gallery/delete_cat/{{$cat->id}}" type="button"
                                               onclick="return confirm('با حذف دسته بندی گالری، تمامی تصاویر آن گالری نیز پاک خواهند شد آیا از این عمل اطمینان دارید؟')"
                                               style="margin-top: 20%;float: left " class="btn btn-danger">حذف دسته
                                                بندی</a>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{$categories->links()}}

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>ثبت دسته بندی جدید
                        </h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form method="POST" action="{{route('panel.gallery.store_cat')}}"
                              class="form-horizontal form-label-left" enctype="multipart/form-data"
                              novalidate="">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">نام
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="name" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">توضیحات
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="desc" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">آپلود تصویر شاخص
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="image" class="file-input" required>

                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>


    </div>

@endsection