@extends('layout.panel.master')
@section('content')
    <style type="text/css">
        /* DivTable.com */
        .divTable {
            display: table;
            width: 100%;
        }

        .divTableRow {
            display: table-row;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
        }

        .divTableCell, .divTableHead {
            border: 0.5px solid #999999;
            display: table-cell;
            padding: 3px 10px;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
            font-weight: bold;
        }

        .divTableFoot {
            background-color: #EEE;
            display: table-footer-group;
            font-weight: bold;
        }

        .divTableBody {
            display: table-row-group;
        }
    </style>
    <style>


        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            /*content: "\f067";*/
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            /*content: "\f00c";*/
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: absolute;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>
    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>نردبان آگهی
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>

                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                        <tr>
                            <th>ردیف</th>
                            <th>کد</th>
                            <th>وضعیت</th>
                            <th>متراژ (متر)</th>
                            <th>عنوان ملک</th>
                            <th>نوع تحویل</th>
                            <th>شهر</th>
                            <th>منطقه</th>
                            <th>نردبان</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($estates as $estate)
                            <tr title="{{$estate->description}}">
                                <td>
                                    {{$i++}}
                                </td>
                                <td>
                                    {{ Str::limit($estate->code, 14 , $end='...') }}
                                </td>

                                <td>
                                    @if($estate->status===1)
                                        <p style="color: green;">فعال</p>
                                    @else
                                        <p style="color: red;">غیرفعال</p>
                                    @endif
                                </td>
                                <td>
                                    {{ Str::limit($estate->area, 28 , $end='...') }}
                                </td>
                                <td>
                                    {{ Str::limit($estate->title, 28 , $end='...') }}
                                </td>

                                <td>
                                    @if($estate->deliverType == "kharid")
                                        <p style="color: green;">خرید</p>
                                    @elseif($estate->deliverType == "foroosh")
                                        <p style="color: blue;">فروش</p>
                                    @elseif($estate->deliverType == "rahn")
                                        <p style="color: red;">رهن</p>
                                    @elseif($estate->deliverType == "ejareh")
                                        <p style="color: yellowgreen;">اجاره</p>
                                    @elseif($estate->deliverType == "kootah")
                                        <p style="color: orange;">کوتاه مدت</p>
                                    @endif
                                </td>

                                <td>
                                    {{$estate->City->name}}
                                </td>

                                <td>
                                    {{$estate->Location->name}}
                                </td>

                                <td>
                                    <button type="button" style="width: 100%" class="col-lg-8 btn btn-info"
                                            data-toggle="modal"
                                            data-target="#show_dialog{{$estate->id}}">
                                        نردبان کردن
                                    </button>
                                </td>


                            </tr>




                            <div class="modal fade" id="show_dialog{{$estate->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">نردبان آگهی</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/panel/ladder/edit/{{$estate->id}}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <select class="form-control js-example-basic-single" name="orderStatus" required>
                                                    <option value="{{$estate->created_at}}">انجام نردبان</option>
                                                </select>
                                                <p>آیا برای نردبان آگهی مطمئن هستید؟</p>
                                                <button type="submit" style="margin-top: 20%; margin-right: 5%;"
                                                        class="btn btn-primary">بله!
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>




                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{$estates->links()}}

            </div>
        </div>


    </div>

@endsection
