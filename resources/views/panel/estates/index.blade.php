@extends('layout.panel.master')
@section('content')
    <style type="text/css">
        /* DivTable.com */
        .divTable {
            display: table;
            width: 100%;
        }

        .divTableRow {
            display: table-row;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
        }

        .divTableCell, .divTableHead {
            border: 0.5px solid #999999;
            display: table-cell;
            padding: 3px 10px;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
            font-weight: bold;
        }

        .divTableFoot {
            background-color: #EEE;
            display: table-footer-group;
            font-weight: bold;
        }

        .divTableBody {
            display: table-row-group;
        }
    </style>
    <style>


        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            /*content: "\f067";*/
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            /*content: "\f00c";*/
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: absolute;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>
    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>املاک
                    </h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                        <tr>
                            <th>ردیف</th>
                            <th>کد</th>
                            <th>وضعیت</th>
                            <th>دسته</th>
                            <th>متراژ (متر)</th>
                            <th>عنوان ملک</th>
                            <th>توضیحات ملک</th>
                            <th>فوری</th>
                            <th>شهر</th>
                            <th>نوع کاربری</th>
                            <th>قیمت به متر (تومان)</th>
                            <th>قیمت کل (تومان)</th>
                            <th>مدیریت</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($estates as $estate)
                            <tr>
                                <td>
                                    {{$i++}}
                                </td>
                                <td>
                                    {{ Str::limit($estate->code, 14 , $end='...') }}
                                </td>
                                <td>
                                    @if($estate->status===1)
                                        <p style="color: green;">فعال</p>
                                    @else
                                        <p style="color: red;">غیرفعال</p>
                                    @endif
                                </td>
                                <td>
                                    {{$estate->Category->name}}
                                </td>
                                <td>
                                    {{ Str::limit($estate->area, 28 , $end='...') }}
                                </td>
                                <td>
                                    {{ Str::limit($estate->title, 28 , $end='...') }}
                                </td>

                                <td title={{$estate->description}}>
                                    {{ Str::limit($estate->description, 28 , $end='...') }}
                                </td>

                                <td>
                                    @if($estate->immediate===1)
                                        <p style="color: green;">بله</p>
                                    @else
                                        <p style="color: red;">خیر</p>
                                    @endif
                                </td>

                                <td>
                                    {{$estate->City->name}}
                                </td>

                                <td>
                                    @if($estate->type==="maskooni")
                                        <p style="color: #008000;">مسکونی</p>
                                    @elseif($estate->type==="sanati")
                                        <p style="color: #008000;">صنعتی</p>
                                    @elseif($estate->type==="edarivatejari")
                                        <p style="color: #008000;">اداری و تجاری</p>
                                    @elseif($estate->type==="keshavarzi")
                                        <p style="color: #008000;">کشاورزی</p>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($estate->price_per_meter))
                                        {{$estate->price_per_meter}}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($estate->price_all))
                                        {{$estate->price_all}}
                                    @endif
                                </td>
                                <td>
                                    <button type="button" style="width: 100%" class="col-lg-8 btn btn-info btn-lg"
                                            data-toggle="modal"
                                            data-target="#edit_estate{{$estate->id}}">
                                        مدیریت
                                    </button>
                                </td>


{{--                                <td>--}}
{{--                                    @if(!empty($estate->image))--}}
{{--                                        <button type="button" style="width: 100%" class="col-lg-8 btn btn-success"--}}
{{--                                                data-toggle="modal"--}}
{{--                                                data-target="#show_pic{{$estate->id}}">--}}
{{--                                            مشاهده تصویر--}}
{{--                                        </button>--}}
{{--                                    @else--}}
{{--                                        <button type="button" style="width: 100%" class="col-lg-8 btn btn-success"--}}
{{--                                                data-toggle="modal"--}}
{{--                                                data-target="#show_pic{{$estate->id}}" disabled="">--}}
{{--                                            تصویر ثبت نشده--}}
{{--                                        </button>--}}

{{--                                    @endif--}}
{{--                                </td>--}}


                            </tr>


                            <div class="modal fade" id="edit_estate{{$estate->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">مدیریت آگهی ملک</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                <form method="POST" action="/panel/estate/edit/{{$estate->id}}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-row">

                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">کد ملک<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <input value="{{$estate->code}}" type="text" name="code" class="form-control"
                                                                       placeholder="کد ملک جدید را وارد کنید..." required>
                                                            </div>
                                                        </div><br><br><br>




                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">وضعیت<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <select name="status"
                                                                        class="form-control" required>

                                                                    @if($estate->status === 1)
                                                                        <option value="1">فعال</option>
                                                                        <option value="0">غیرفعال</option>
                                                                    @else
                                                                        <option value="0">غیرفعال</option>
                                                                        <option value="1">فعال</option>
                                                                    @endif

                                                                </select>
                                                            </div>
                                                        </div><br><br>


                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">انتخاب دسته بندی<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control js-example-basic-single" name="category_id" required>
                                                                    <option selected value="" style="color: red;">انتخاب کنید</option>
                                                                    @foreach($cats as $cat)
                                                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div><br><br>



                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">متراژ<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                            <input name="area" type="text" class="form-control"
                                                                   value="{{$estate->area}}"
                                                                   placeholder="متراژ جدید ملک را وارد کنید" required>
                                                            </div>
                                                        </div><br><br>


                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">عنوان ملک<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                            <input name="title" type="text" class="form-control"
                                                                   value="{{$estate->title}}"
                                                                   placeholder="عنوان جدید ملک را وارد کنید" required>
                                                            </div>
                                                        </div><br><br>



                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">توضیحات<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <textarea placeholder="توضیحات ملک را اینجا تایپ کنید..." class="form-control" rows="4" cols="100" required name="description">{{$estate->description}}</textarea>




                                                        </div>
                                                        </div><br><br><br><br><br>


                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">فوری<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control js-example-basic-single" name="immediate" required>
                                                                    @if($estate->immediate === 1)
                                                                        <option value="1">بله</option>
                                                                        <option value="0">خیر</option>
                                                                    @else
                                                                        <option value="0">خیر</option>
                                                                        <option value="1">بله</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div><br><br>


                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">انتخاب شهر<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control js-example-basic-single" name="city_id" required>
                                                                    <option selected value="" style="color: red;">انتخاب کنید</option>
                                                                    @foreach($cities as $city)
                                                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div><br><br>




                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">انتخاب منطقه<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control js-example-basic-single" name="location_id" required>
                                                                    <option selected value="" style="color: red;">انتخاب کنید</option>
                                                                    @foreach($locations as $location)
                                                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div><br><br>




                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">کاربری<span style="color:red; ">*</span></label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="type" required>
                                                                @if($estate->type === "maskooni")
                                                                    <option value="maskooni">مسکونی</option>
                                                                    <option value="sanati">صنعتی</option>
                                                                    <option value="edarivatejari">اداری و تجاری</option>
                                                                    <option value="keshavarzi">کشاورزی</option>
                                                                    <option value="baghi">باغی</option>
                                                                @elseif(($estate->type === "sanati"))
                                                                    <option value="sanati">صنعتی</option>
                                                                    <option value="maskooni">مسکونی</option>
                                                                    <option value="edarivatejari">اداری و تجاری</option>
                                                                    <option value="keshavarzi">کشاورزی</option>
                                                                    <option value="baghi">باغی</option>
                                                                @elseif($estate->type === "edarivatejari")
                                                                    <option value="edarivatejari">اداری و تجاری</option>
                                                                    <option value="maskooni">مسکونی</option>
                                                                    <option value="sanati">صنعتی</option>
                                                                    <option value="keshavarzi">کشاورزی</option>
                                                                    <option value="baghi">باغی</option>

                                                                @elseif($estate->type === "keshavarzi")
                                                                    <option value="keshavarzi">کشاورزی</option>
                                                                    <option value="maskooni">مسکونی</option>
                                                                    <option value="sanati">صنعتی</option>
                                                                    <option value="edarivatejari">اداری و تجاری</option>
                                                                    <option value="baghi">باغی</option>
                                                                @elseif($estate->type === "baghi")
                                                                    <option value="baghi">باغی</option>
                                                                    <option value="maskooni">مسکونی</option>
                                                                    <option value="sanati">صنعتی</option>
                                                                    <option value="edarivatejari">اداری و تجاری</option>
                                                                    <option value="keshavarzi">کشاورزی</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>




                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">سن بنا</label>
                                                            <div class="col-lg-10">
                                                        <input name="age" type="text" class="form-control"
                                                               value="{{$estate->age}}"
                                                               placeholder="سن جدید ملک را وارد کنید">
                                                            </div>
                                                        </div><br><br>



                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">آسانسور</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="elevator">
                                                                <option value="">-</option>
                                                                @if($estate->elevator === 1)
                                                                    <option value="1">دارد</option>
                                                                    <option value="0">ندارد</option>
                                                                @else
                                                                    <option value="0">ندارد</option>
                                                                    <option value="1">دارد</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>


                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">پارکینگ</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="parking">
                                                                <option value="">-</option>
                                                                @if($estate->parking === 1)
                                                                    <option value="1">دارد</option>
                                                                    <option value="0">ندارد</option>
                                                                @else
                                                                    <option value="0">ندارد</option>
                                                                    <option value="1">دارد</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>


                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">انبار</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="stockroom">
                                                                <option value="">-</option>
                                                                @if($estate->stockroom === 1)
                                                                    <option value="1">دارد</option>
                                                                    <option value="0">ندارد</option>
                                                                @else
                                                                    <option value="0">ندارد</option>
                                                                    <option value="1">دارد</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>


                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">پکیج</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="package">
                                                                <option value="">-</option>
                                                                @if($estate->package === 1)
                                                                    <option value="1">دارد</option>
                                                                    <option value="0">ندارد</option>
                                                                @else
                                                                    <option value="0">ندارد</option>
                                                                    <option value="1">دارد</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>




                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">تعداد اتاق</label>
                                                            <div class="col-lg-10">
                                                                <select class="form-control js-example-basic-single" name="rooms">
                                                                    @if($estate->rooms == "")
                                                                        <option value="">-</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    @elseif($estate->rooms === "1")
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    @elseif(($estate->rooms === "2"))
                                                                        <option value="2">2</option>
                                                                        <option value="1">1</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                    @elseif($estate->rooms === "3")
                                                                        <option value="3">3</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>

                                                                    @elseif($estate->rooms === "4")
                                                                        <option value="4">4</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="5">5</option>
                                                                    @elseif($estate->rooms === "5")
                                                                        <option value="5">5</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div><br><br>




                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">رهن</label>
                                                            <div class="col-lg-10">
                                                        <input name="mortgage" type="text" class="form-control"
                                                               value="{{$estate->mortgage}}"
                                                               placeholder="مبلغ رهن جدید را وارد کنید">
                                                            </div>
                                                        </div><br><br>

                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">اجاره</label>
                                                            <div class="col-lg-10">
                                                        <input name="rent" type="text" class="form-control"
                                                               value="{{$estate->rent}}"
                                                               placeholder="مبلغ اجاره جدید را وارد کنید">
                                                            </div>
                                                        </div><br><br>


                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">قابلیت تبدیل</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control js-example-basic-single" name="change">
                                                                <option value="">-</option>
                                                                @if($estate->change === 1)
                                                                    <option value="1">دارد</option>
                                                                    <option value="0">ندارد</option>
                                                                @else
                                                                    <option value="0">ندارد</option>
                                                                    <option value="1">دارد</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div><br><br>



                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">قیمت متر مربع</label>
                                                            <div class="col-lg-10">
                                                        <input name="price_per_meter" type="text" class="form-control"
                                                               value="{{$estate->price_per_meter}}"
                                                               placeholder="قیمت هر متر مربع جدید را وارد کنید">
                                                            </div>
                                                        </div><br><br>



                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">قیمت کل</label>
                                                            <div class="col-lg-10">
                                                        <input name="price_all" type="text" class="form-control"
                                                               value="{{$estate->price_all}}"
                                                               placeholder="قیمت کل جدید را وارد کنید">
                                                            </div>
                                                        </div><br><br>




{{--                                                    <div class="form-row">--}}
{{--                                                        <label class="col-lg-2 control-label text-semibold">تغییر--}}
{{--                                                            تصویر:</label>--}}
{{--                                                        <br/>--}}
{{--                                                        <input type="file" name="image" class="file-input">--}}
{{--                                                    </div>--}}

                                                        <button type="submit" style="margin-top: 20%; margin-right: 5%;"
                                                                class="btn btn-primary">ثبت تغییرات
                                                        </button>
                                                </form>




                                                <a href="/panel/estate/delete/{{$estate->id}}" type="button"
                                                   onclick="return confirm('آیا از این عمل مطمئن هستید؟')"
                                                   style="margin-top: 20%;float: right " class="btn btn-danger form-group">حذف
                                                    ملک</a>


                                                <br/>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary form-group"
                                                            data-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



{{--                            <div class="modal fade" id="show_pic{{$estate->id}}" tabindex="-1" role="dialog"--}}
{{--                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">--}}
{{--                                <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                                    <div class="modal-content">--}}
{{--                                        <div class="modal-header">--}}
{{--                                            <h5 class="modal-title" id="exampleModalLongTitle">تصویر محصول</h5>--}}
{{--                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                <span aria-hidden="true">&times;</span>--}}
{{--                                            </button>--}}
{{--                                        </div>--}}
{{--                                        <div class="modal-body">--}}
{{--                                            <div>--}}
{{--                                                <div class="text-center">--}}
{{--                                                    <img src="/images/{{$estate->image}}"--}}
{{--                                                         class="img-thumbnail rounded ">--}}
{{--                                                </div>--}}
{{--                                                <br/>--}}
{{--                                                <div class="modal-footer">--}}
{{--                                                    <button type="button" class="btn btn-secondary"--}}
{{--                                                            data-dismiss="modal">--}}
{{--                                                        بستن--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


{{--                            </div>--}}




                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{$estates->links()}}

            </div>
        </div>


    </div>

@endsection
