@extends('layout.panel.master')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script src="{{asset('assets/js/customize-widgets.js')}}"></script>

    <script src="{{asset('assets/js/jquery.js')}}"></script>

    <script src="{{asset('assets/js/jquery-1.8.2.min.js')}}"></script>

    <script src="{{asset('assets/js/jquery-3.4.1.js')}}"></script>




    {{--    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>--}}
{{--    <script src="public/assets/js/select2.min.js"></script>--}}

{{--    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>--}}

    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>ثبت ملک جدید
                    </h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="x_content">

                    <form method="post" action="{{route('panel.estate.store')}}"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="content-group">


                            <div class="form-group">
                                <label class="control-label col-lg-2">کد ملک <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="code" class="form-control"
                                           placeholder="کد ملک را وارد کنید..." required>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">وضعیت <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="status" required>
                                            <option value="1">فعال</option>
                                            <option value="0">غیرفعال</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">نوع تحویل <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="deliverType" required>
                                        <option value="kharid">خرید</option>
                                        <option value="foroosh">فروش</option>
                                        <option value="rahn">رهن</option>
                                        <option value="ejareh">اجاره</option>
                                        <option value="kootah">اجاره کوتاه مدت</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">انتخاب دسته بندی <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="category_id" required>
                                        @foreach($cats as $cat)
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">متراژ <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="area" class="form-control"
                                           placeholder="متراژ را اینجا تایپ کنید..." required>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">عنوان ملک <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" name="title" class="form-control"
                                           placeholder="عنوان ملک را اینجا تایپ کنید..." required>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">توضیحات ملک<span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <textarea placeholder="توضیحات ملک را اینجا تایپ کنید..." required name="description" class="form-control" rows="4" cols="100"></textarea>

                                </div>
                            </div><br><br><br><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">ویژه<span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="immediate" required>
                                        <option value="0">خیر</option>
                                        <option value="1">بله</option>
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">شهر <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="city_id" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><br><br>

                            <div class="form-group">
                                <label class="control-label col-lg-2">منطقه <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="location_id" required>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">نوع کاربری <span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="type" required>
                                        <option value="maskooni">مسکونی</option>
                                        <option value="sanati">صنعتی</option>
                                        <option value="edarivatejari">اداری و تجاری</option>
                                        <option value="keshavarzi">کشاورزی</option>
                                        <option value="baghi">باغی</option>
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">فروخته<span style="color:red; ">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="isSell" required>
                                        <option value="0">نشده</option>
                                        <option value="1">شده</option>
                                    </select>
                                </div>
                            </div><br><br>




                            <div class="form-group">
                                <label class="control-label col-lg-2">سن بنا (خانه، آپارتمان، ویلا)</label>
                                <div class="col-lg-10">
                                    <input type="text" name="age" class="form-control"
                                           placeholder="سن بنا را اینجا تایپ کنید...">
                                </div>
                            </div><br><br>





                            <div class="form-group">
                                <label class="control-label col-lg-2">پارکینگ (خانه، آپارتمان، ویلا)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="parking">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">انبار (خانه، آپارتمان، ویلا)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="stockroom">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">پکیج (خانه، آپارتمان، ویلا)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="package">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>





                            <div class="form-group">
                                <label class="control-label col-lg-2">کابینت (خانه، ویلا، آپارتمان)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="cabinet">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">تراس (خانه، ویلا، آپارتمان)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="terrace">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>




                            <div class="form-group">
                                <label class="control-label col-lg-2">تعداد اتاق (خانه، آپارتمان، ویلا)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="rooms">
                                        <option value="">-</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div><br><br>




                            <div class="form-group">
                                <label class="control-label col-lg-2">آسانسور (آپارتمان)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="elevator">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">سند (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="document">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد (قباله ای)</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">مجوز ساخت (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="license">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>




                            <div class="form-group">
                                <label class="control-label col-lg-2">کنتور آب (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="water">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">کنتور برق (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="electricity">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">کنتور گاز (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="gas">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">درب کنترلی (عمومی)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="remote">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">چاه آب (کشاورزی و باغ)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="waterWell">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">فنس (کشاورزی و باغ)</label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="fence">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>






                            <div class="form-group">
                                <label class="control-label col-lg-2">رهن </label>
                                <div class="col-lg-10">
                                    <input type="text" name="mortgage" class="form-control"
                                           placeholder="مبلغ رهن را اینجا تایپ کنید...">
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">اجاره </label>
                                <div class="col-lg-10">
                                    <input type="text" name="rent" class="form-control"
                                           placeholder="مبلغ اجاره را اینجا تایپ کنید...">
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">قابلیت تبدیل </label>
                                <div class="col-lg-10">
                                    <select class="form-control js-example-basic-single" name="change">
                                        <option value="">-</option>
                                        <option value="1">دارد</option>
                                        <option value="0">ندارد</option>
                                    </select>
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">قیمت هر متر (خرید و فروش)</label>
                                <div class="col-lg-10">
                                    <input type="text" name="price_per_meter" class="form-control"
                                           placeholder="قیمت هر متر مربع را اینجا تایپ کنید...">
                                </div>
                            </div><br><br>


                            <div class="form-group">
                                <label class="control-label col-lg-2">قیمت کل (خرید و فروش)</label>
                                <div class="col-lg-10">
                                    <input type="text" name="price_all" class="form-control"
                                           placeholder="قیمت کل را اینجا تایپ کنید...">
                                </div>
                            </div><br><br>



                            <div class="form-group">
                                <label class="control-label col-lg-2">آپلود تصویر<span style="color:red; ">*</span></label>

                                <div class="col-lg-10">
                                    <input value="upload" id="main_pic" style="display:inline" class="btn-blue" name="gallery_pic[]" type="file" accept="image/*" >
                                    <img style="display:none" id="output" src="" width="165" height="100"><br>
                                    <span id="img_err" style="color:red;display:none;">اندازه تصویر باید حداکثر 300*500 باشد</span>

                                    <br/>

                                    <div id="add_pic_below"></div>

                                    <button class="form-control btn-blue" type="button" id="add_pic">اضافه کردن تصویر
                                    </button>
                                </div>
                            </div><br><br>



                        </fieldset>

                        <br/> <br/>
                        <hr/>
                        <br/>
                        <button type="submit" class="btn btn-success form-control">ثبت
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>




    <script>
        $('document').ready(() => {

            var _URL = window.URL || window.webkitURL;
            $('#main_pic').change(()=>{
                var file = $('#main_pic')[0].files[0];
                img = new Image();
                var imgwidth = 0;
                var imgheight = 0;
                var maxwidth = 500;
                var maxheight = 300;

                img.src = _URL.createObjectURL(file)

                img.onload = function() {
                    imgwidth = this.width;
                    imgheight = this.height;

                    $("#width").text(imgwidth);
                    $("#height").text(imgheight);
                    if(imgwidth <= maxwidth && imgheight <= maxheight){
                        $('#img_err').hide(400);
                        document.getElementById('output').src = img.src;
                        $('#output').show(400);

                    }else{
                        $('#img_err').show(500);
                        $('#main_pic').val('');
                        $('#output').hide(200);

                        //empty value

                    }

                }

            });



            var count=1;

            $('#add_pic').click((e) => {
                count++;
                $('#add_pic_below').append('<input style="display:inline" name="gallery_pic[]" value="upload"'+
                    'type="file" accept="image/*" onchange="document.getElementById(\'picbox'+count+'\').src = window.URL.createObjectURL(this.files[0])">'
                    +'<img id="picbox'+count+'" src="" width="165" height="100">'+'<br/><br/>');
            });

        })

        $('.js-example-basic-single').select2();
    </script>


@endsection





