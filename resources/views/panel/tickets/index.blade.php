@extends('layout.panel.master')
@section('content')
    <style type="text/css">
        /* DivTable.com */
        .divTable {
            display: table;
            width: 100%;
        }

        .divTableRow {
            display: table-row;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
        }

        .divTableCell, .divTableHead {
            border: 0.5px solid #999999;
            display: table-cell;
            padding: 3px 10px;
        }

        .divTableHeading {
            background-color: #EEE;
            display: table-header-group;
            font-weight: bold;
        }

        .divTableFoot {
            background-color: #EEE;
            display: table-footer-group;
            font-weight: bold;
        }

        .divTableBody {
            display: table-row-group;
        }
    </style>
    <style>


        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            /*content: "\f067";*/
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            /*content: "\f00c";*/
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: absolute;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>
    <div class="right_col" role="main">
        <div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>تیکت های پشتیبانی
                    </h2>
                    {{--<p>نمایش سفارشات</p>--}}
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    @if (Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="x_content">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                        <tr>
                            <th>ردیف</th>
                            <th>نام</th>
                            <th>ایمیل</th>
                            <th>شماره تماس</th>
                            <th>وضعیت</th>
                            <th>متن پیام</th>
                            <th>مدیریت</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <td>
                                    {{$i++}}
                                </td>
                                <td>
                                    {{$ticket->sender_name}}
                                </td>
                                <td>
                                    {{$ticket->sender_email}}

                                </td>
                                <td>
                                    {{$ticket->sender_phone}}
                                </td>
                                <td>
                                    @if($ticket->status === 1)
                                        جدید
                                    @else
                                        پاسخ داده شده
                                    @endif
                                </td>
                                <td>
                                    <button type="button" style="width: 100%" class="col-lg-8 btn btn-success"
                                            data-toggle="modal"
                                            data-target="#show_desc{{$ticket->id}}">
                                       متن پیام
                                    </button>
                                </td>
                                <td>
                                    <button type="button" style="width: 100%" class="col-lg-8 btn btn-success"
                                            data-toggle="modal"
                                            data-target="#show_manage{{$ticket->id}}">
                                        مدیریت
                                    </button>
                                </td>
                            </tr>

                            <div class="modal fade" id="show_manage{{$ticket->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">مدیریت تیکت</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                <form method="POST" action="/panel/ticket/send_reply/{{$ticket->id}}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-row">

                                                        <div class="form-group">
                                                            <label class="control-label col-lg-2">ارسال پاسخ<span style="color:red; ">*</span></label>
                                                            <div class="col-lg-10">
                                                                <textarea placeholder="پاسخ مدنظرتان را بنویسید" required name="reply" class="form-control" rows="4" cols="100"></textarea>

                                                            </div>
                                                        </div>


                                                    </div>

                                                    <button type="submit" style="margin-top: 20%; margin-right: 5%;"
                                                            class="btn btn-primary">ارسال
                                                    </button>
                                                </form>
                                                <a href="/panel/ticket/delete_ticket/{{$ticket->id}}" type="button"
                                                   onclick="return confirm('آیا از این عمل مطمئن هستید؟')"
                                                   style="margin-top: 20%;float: right " class="btn btn-danger">حذف
                                                    تیکت</a>

                                                <br/>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">
                                                        بستن
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="modal fade" id="show_desc{{$ticket->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">متن تیکت</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>

                                                {{$ticket->message}}
                                            </p>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{$tickets->links()}}

            </div>
        </div>


    </div>

@endsection
