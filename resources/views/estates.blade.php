<!DOCTYPE html>
<html>
<head>
    @include('sections.head')
</head>

<body>

<header class="topeleman">
    @include('sections.header')
</header>

<div class="clear"></div>

<main class="maineleman">
    <section class="filterbox">
{{--        <div class="pardispic">--}}
{{--            <img src="/assets/images/pardis2.png">--}}
{{--        </div>--}}
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="clear"></div>


            <div class="filterelemanbox">
                <label class="labeltitle">شهر</label>
                <select class="filterselect">
                    <option value="">همه</option>
                    <option value="1">دارد</option>
                    <option value="0">ندارد</option>
                </select>
            </div>




        <div class="filterelemanbox">
            <label class="labeltitle">منطقه</label>
            <select class="filterselect">
                <option value="">همه</option>
                <option value="1">دارد</option>
                <option value="0">ندارد</option>
            </select>
        </div>

        <div class="filterelemanbox">
            <label class="labeltitle">نوع کاربری</label>
            <select class="filterselect">
                <option value="">همه</option>
                <option value="1">دارد</option>
                <option value="0">ندارد</option>
            </select>
        </div>

        <div class="filterelemanbox">
            <label class="labeltitle">سن بنا</label>
            <select class="filterselect">
                <option value="">همه</option>
                <option value="1">دارد</option>
                <option value="0">ندارد</option>
            </select>
        </div>


        <div class="filterelemanboxelemans">
            <label class="labeltitle">امکانات</label>

            <div style="width: 100%; height: auto;">

                <div class="checkboxitems">
                    <label class="toggle">
                        <input type="checkbox" name="t1">&nbsp&nbsp&nbspپارکینگ
                    </label>
                </div>

                <div class="checkboxitems">
                    <label class="toggle">
                        <input type="checkbox" name="t2">&nbsp&nbsp&nbspانبار
                    </label>
                </div>


                <div class="checkboxitems">
                    <label class="toggle">
                        <input type="checkbox" name="t3">&nbsp&nbsp&nbspآسانسور
                    </label>
                </div>

                <div class="checkboxitems">
                    <label class="toggle">
                        <input type="checkbox" name="t4">&nbsp&nbsp&nbspپکیج
                    </label>
                </div>

                <div class="checkboxitems">
                    <label class="toggle">
                        <input type="checkbox" name="t5">&nbsp&nbsp&nbspسند دار
                    </label>
                </div>

            </div>


        </div>



        <div class="filterelemanbox">
            <label class="labeltitle">رهن</label>
            <label class="labeltitle50">از</label>
            <input type="text" class="filterselect50" style="margin-left: 4%">
            <label class="labeltitle50">تا</label>

            <input type="text" class="filterselect50">
        </div>

        <div class="filterelemanbox">
            <label class="labeltitle">اجاره</label>
            <label class="labeltitle50">از</label>
            <input type="text" class="filterselect50" style="margin-left: 4%">
            <label class="labeltitle50">تا</label>

            <input type="text" class="filterselect50">
        </div>


        <div class="filterelemanbox">
            <label class="labeltitle">قیمت کل</label>
            <label class="labeltitle50">از</label>
            <input type="text" class="filterselect50" style="margin-left: 4%">
            <label class="labeltitle50">تا</label>

            <input type="text" class="filterselect50">
        </div>


        <div class="filterelemanbox" style="float: left">
            <button class="filterbutton">جستجو</button>
        </div>
    </section>

    <div class="clear"></div>
    <br><br><br><br><br>

    <section class="resultbox">



        <div class="container">
            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000">ویـــژه</p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/sample.jpg">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000">ویـــژه</p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/sample.jpg">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000">ویـــژه</p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/sample.jpg">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000">ویـــژه</p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/sample.jpg">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>

            <a href="#">
                <div class="estatebox">
                    <div>
                        <p style="float: left; color: #ff0000"></p>
                        <p style="float: right">کد 10000</p>
                    </div>
                    <img src="/assets/images/house.png">
                    <p>عنوان ملک</p>
                    <p style="font-weight: normal; font-size: 12px">توضیحات ملک...                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد.
                    </p>

                </div>
            </a>




        </div>


        @include('sections.showdialog')


    </section>
    <div>
        <img src="/assets/images/Bottom.jpg" style="width: 100%; height: 200px; ">
    </div>
</main>

<div class="clear"></div>

<footer class="bottomeleman">
    @include('sections.footer')
</footer>

</body>





<script>


    jQuery(document).ready(function() {
        jQuery('.toggle-nav').click(function(e) {
            jQuery(this).toggleClass('active');
            jQuery('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });




    $(document).ready(function() {
        $(document).scroll(function(){
            x = $(document).scrollTop();
            if ( x > 198 ) {
                $(".topbar").addClass("topfix");
            }else{
                $(".topbar").removeClass("topfix"); }
        });
    });
</script>


</html>
