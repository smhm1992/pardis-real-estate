<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="fontiran.com:license" content="Y68A9">
    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/ico"/>
    <title>پنل مدیریت  </title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/nprogress.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-progressbar-3.3.4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/green.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.min.css')}}">
    <script src="{{asset('assets/js/customize-widgets.js')}}"></script>
</head>
<!-- /header content -->
<body class="nav-md .">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col hidden-print">
            @include('layout.panel.menu')
        </div>

        <!-- top navigation -->
        <div class="top_nav hidden-print">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
{{--                                <img src="{{asset('assets/images/img.jpg')}}" alt="">مرتضی کریمی--}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> خروج</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <!-- /header content -->

        <!-- page content -->
@yield('content')
        <!-- /page content -->

        <!-- footer content -->
        <footer class="hidden-print">
            <div class="pull-left">
                {{--Gentelella - قالب پنل مدیریت بوت استرپ <a href="https://colorlib.com">Colorlib</a> | پارسی شده توسط <a--}}
                        {{--href="https://morteza-karimi.ir">مرتضی کریمی</a>--}}
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<div id="lock_screen">
    <table>
        <tr>
            <td>
                <div class="clock"></div>
                <span class="unlock">
                    <span class="fa-stack fa-5x">
                      <i class="fa fa-square-o fa-stack-2x fa-inverse"></i>
                      <i id="icon_lock" class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                </span>
            </td>
        </tr>
    </table>
</div>
<!-- jQuery -->

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/nprogress.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('assets/js/icheck.min.js')}}"></script>
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/Chart.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/js/gauge.min.js')}}"></script>
<script src="{{asset('assets/js/skycons.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.pie.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.time.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.stack.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.resize.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{asset('assets/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{asset('assets/js/curvedLines.js')}}"></script>
<script src="{{asset('assets/js/date.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.vmap.js')}}"></script>
<script src="{{asset('assets/js/jquery.vmap.world.js')}}"></script>
<script src="{{asset('assets/js/jquery.vmap.sampledata.js')}}"></script>
<script src="{{asset('assets/js/custom.min.js')}}"></script>



</body>
</html>
