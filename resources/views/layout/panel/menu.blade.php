<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>پنل مدیریت </span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        {{--<div class="profile_pic">--}}
            {{--<img src="#" alt="..." class="img-circle profile_img">--}}
        {{--</div>--}}
        <div class="profile_info">
            <span>خوش اومدی فرشاد</span>
            <br/>
            {{--{{\Illuminate\Support\Facades\Auth::user()->f_name}}--}}

        </div>
    </div>
    <!-- /menu profile quick info -->

    <br/>

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            {{--admin--}}
            <h3>پنل مدیریت</h3>
            <ul class="nav side-menu">
                <li><a href="/panel/"><i class="fa fa-windows"></i> داشبورد </a>
                </li>


                <li><a><i class="fa fa-circle-o"></i> املاک<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
{{--                        <li><a href="{{route('panel.estate.est_index')}}">مدیریت املاک</a></li>--}}
                        <li><a href="{{route('panel.estate.est_create')}}">ثبت ملک جدید </a>
                        </li>
                        <li><a href="{{route('panel.estate.est_search')}}">جستجوی ملک</a>
                        </li>
                    </ul>

                </li>



                <li><a><i class="fa fa-home"></i> خانه و ویلا<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.house.est_index')}}">مدیریت املاک خانه و ویلایی</a></li>
{{--                        <li><a href="#">ثبت خانه و ویلای جدید </a>--}}
                        </li>
                    </ul>
                </li>



                <li><a><i class="fa fa-building-o"></i> آپارتمان<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.apartment.est_index')}}">مدیریت املاک آپارتمانی</a></li>
{{--                        <li><a href="#">ثبت آپارتمان جدید </a>--}}
                        </li>
                    </ul>
                </li>


                <li><a><i class="fa fa-tree"></i> کشاورزی و باغ<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.agriculture.est_index')}}">مدیریت املاک کشاورزی و باغ</a></li>
{{--                        <li><a href="#">ثبت زمین کشاورزی و باغ جدید </a>--}}
                        </li>
                    </ul>
                </li>


                <li><a><i class="fa fa-map"></i> زمین<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.earth.est_index')}}">مدیریت املاک زمین</a></li>
{{--                        <li><a href="#">ثبت زمین جدید </a>--}}
                        </li>
                    </ul>
                </li>


                <li><a><i class="fa fa-wrench"></i> تجاری و صنعتی<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.commercial.est_index')}}">مدیریت املاک تجاری و صنعتی</a></li>
{{--                        <li><a href="#">ثبت ملک تجاری و صنعتی جدید </a>--}}
                        </li>
                    </ul>
                </li>




                <li><a><i class="fa fa-group"></i> دسته بندی<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.category.cat_index')}}">مدیریت دسته بندی</a></li>
                        <li><a href="{{route('panel.category.cat_create')}}">ثبت دسته جدید</a>
                        </li>
                    </ul>
                </li>



                <li><a><i class="fa fa-map-o"></i> شهرها<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.city.city_index')}}">مدیریت شهرها</a></li>
                        <li><a href="{{route('panel.city.city_create')}}">ثبت شهر جدید</a>
                        </li>
                    </ul>
                </li>



                <li><a><i class="fa fa-map-marker"></i> منطقه<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li><a href="{{route('panel.location.loc_index')}}">مدیریت منطقه</a></li>
                        <li><a href="{{route('panel.location.loc_create')}}">ثبت منطقه جدید</a>
                        </li>
                    </ul>
                </li>


                <li><a href="{{route('panel.ladder.index')}}"><i class="fa fa-arrow-circle-up"></i>نردبان آگهی</a>
                </li>


                <li><a href="{{route('panel.order.index')}}"><i class="fa fa-briefcase"></i>درخواست های کارشناسی ملک</a>
                </li>






{{--                <li><a><i class="fa fa-picture-o"></i> گالری تصاویر<span class="fa fa-chevron-down"></span></a>--}}
{{--                    <ul class="nav child_menu" style="display: none;">--}}
{{--                        <li><a href="{{route('panel.gallery.index')}}">مدیریت</a></li>--}}
{{--                        --}}{{--<li><a href="#">مشاهده سوابق مالی</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

                {{--<li><a><i class="fa fa-group"></i> اطلاعیه ها<span class="fa fa-chevron-down"></span></a>--}}
                    {{--<ul class="nav child_menu" style="display: none;">--}}
                        {{--<li><a href="{{route('panel.notice.index')}}">نمایش اطلاعیه ها</a></li>--}}
                        {{--<li><a href="{{route('panel.notice.create')}}">ثبت اطلاعیه جدید </a></li>--}}
                        {{--<li><a href="#">مشاهده سوابق مالی</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li><a><i class="fa fa-group"></i> همکاران<span class="fa fa-chevron-down"></span></a>--}}
                    {{--<ul class="nav child_menu" style="display: none;">--}}
                        {{--<li><a href="{{route('panel.teammate.indextm')}}">نمایش همکاران</a></li>--}}
                        {{--<li><a href="{{route('panel.teammate.createtm')}}">ثبت همکار جدید </a></li>--}}
                        {{--<li><a href="#">مشاهده سوابق مالی</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li><a><i class="fa fa-group"></i> گواهینامه ها<span class="fa fa-chevron-down"></span></a>--}}
                    {{--<ul class="nav child_menu" style="display: none;">--}}
                        {{--<li><a href="{{route('panel.certificate.indexcer')}}">نمایش گواهینامه ها</a></li>--}}
                        {{--<li><a href="{{route('panel.certificate.createcer')}}">ثبت گواهینامه جدید </a></li>--}}
                        {{--<li><a href="#">مشاهده سوابق مالی</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li><a href="{{route('panel.order.index')}}"><i class="fa fa-paper-plane-o"></i> سفارشات</a>--}}
                {{--</li>--}}
                <li><a href="{{route('panel.ticket.panel_index')}}"><i class="fa fa-paper-plane-o"></i> پشتیبانی</a>
                </li>



            </ul>
        </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        {{--<a data-toggle="tooltip" data-placement="top" title="تنظیمات">--}}
        {{--<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>--}}
        {{--</a>--}}
        <a data-toggle="tooltip" data-placement="top" title="تمام صفحه" onclick="toggleFullScreen();">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="قفل" class="lock_btn">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="خروج">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
</div>
