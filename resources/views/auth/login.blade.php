<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>صفحه ورود | </title>


    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.rtl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/nprogress.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/green.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.min.css')}}">

</head>

<body class="login">
<div>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form  method="POST" action="{{ route('login') }}">
                    @csrf
                    <h1>ورود به پنل ادمین</h1>
                    <div>
                        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="ایمیل" required="" />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="رمز ورود" required="" />
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div>
                        <button type="submit" class="btn btn-success">ورود</button>
                    </div>

                    <div class="clearfix"></div>


                </form>
            </section>
        </div>

    </div>
</div>
</body>
</html>
