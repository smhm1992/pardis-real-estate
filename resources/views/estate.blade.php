<!DOCTYPE html>
<html>
<head>
    @include('sections.head')
    <link rel="stylesheet" type="text/css" href="/assets/slider/css/slider.css">
    <script src="/assets/slider/js/jquery.flexslider-min.js"></script>

</head>

<body>

<header class="topeleman">
    @include('sections.header')
</header>

<div class="clear"></div>

<main class="maineleman">
    <section class="filterbox" style="height: 200px;">
{{--        <div class="pardispic">--}}
{{--            <img src="/assets/images/pardis2.png">--}}
{{--        </div>--}}
        <div class="clear"></div>
    </section>

    <div class="clear"></div>

    <section class="resultbox" style="height: 900px;">

        <div class="container">
            <div class="contentbox">
                <div class="sliderbox">

                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="/assets/images/apartmant.png" />
                            </li>

                            <li>
                                <img src="/assets/images/house.png" />
                            </li>

                            <li>
                                <img src="/assets/images/sample.jpg" />
                            </li>
                        </ul>
                    </div>

                </div>


                <div class="propertiesbox">
                    <div class="propertiesboxelemans">
                        <p style="float: right; font-size: 18px; font-weight: bold">عنوان ملک</p>
                        <p style="float: left; font-size: 18px; font-weight: bold">کد 10000</p>

                    </div>
                    <br><br>
                    <div class="propertiesboxelemans">
                        <p style="float: right">قیمت کل</p>
                        <p style="float: left">100000 تومان</p>
                    </div>
                    <br>
                    <hr>

                    <div class="propertiesboxelemans">
                        <p style="float: right">قیمت کل</p>
                        <p style="float: left">100000 تومان</p>
                    </div>
                    <br>
                    <hr>

                    <div class="propertiesboxelemans">
                        <p style="float: right">قیمت کل</p>
                        <p style="float: left">100000 تومان</p>
                    </div>
                    <br>
                    <hr>

                    <div class="propertiesboxelemans">
                        <p style="float: right;">قیمت کل</p>
                        <p style="float: left;">100000 تومان</p>
                    </div>
                    <br>
                    <hr>

                    <div class="propertiesboxelemans">
                        <p style="float: right">قیمت کل</p>
                        <p style="float: left">100000 تومان</p>
                    </div>
                    <br>
                    <hr>

                    <div class="propertiesboxelemans">
                        <p style="float: right">قیمت کل</p>
                        <p style="float: left">100000 تومان</p>
                    </div>
                    <br>


                </div>
            </div>


            <div class="descriptionbox">
                <div class="descriptionboxleft">
                    <p>                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می‌طلبد تا با نرم‌افزارها شناخت بیشتری را برای طراحان رایانه ای علی‌الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می‌توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساساً مورد استفاده قرار گیرد.
                    </p>
                </div>

                <div class="descriptionboxright">
                    <P>امکانات...</P>
                </div>
            </div>
        </div>
        @include('sections.showdialog')

    </section>
    <div>
        <img src="/assets/images/Bottom.jpg" style="width: 100%; height: 200px; ">
    </div>
</main>

<div class="clear"></div>

<footer class="bottomeleman">
    @include('sections.footer')
</footer>

</body>





<script>


    $(document).ready(function () {
        $('.flexslider').flexslider({
            animation: 'fade',
            controlsContainer: '.flexslider'
        });
    });


    jQuery(document).ready(function() {
        jQuery('.toggle-nav').click(function(e) {
            jQuery(this).toggleClass('active');
            jQuery('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });




    $(document).ready(function() {
        $(document).scroll(function(){
            x = $(document).scrollTop();
            if ( x > 198 ) {
                $(".topbar").addClass("topfix");
            }else{
                $(".topbar").removeClass("topfix"); }
        });
    });
</script>


</html>
