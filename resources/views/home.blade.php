<!DOCTYPE html>
<html>
<head>
    @include('sections.head')
</head>

<body>

    <header class="topeleman">
        @include('sections.header')
    </header>
<br>
    <div class="clear"></div>

    <main class="maineleman">
{{--        @include('sections.filterbox')--}}
        @include('sections.mainside')
    </main>

    <div class="clear"></div>

    <footer class="bottomeleman">
        @include('sections.footer')
    </footer>

</body>









<script>
    // $(document).ready(function () {
    //     $('.flexslider').flexslider({
    //         animation: 'fade',
    //         controlsContainer: '.flexslider'
    //     });
    // });




    jQuery(document).ready(function() {
        jQuery('.toggle-nav').click(function(e) {
            jQuery(this).toggleClass('active');
            jQuery('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });




    $(document).ready(function() {
        $(document).scroll(function(){
            x = $(document).scrollTop();
            if ( x > 198 ) {
                $(".topbar").addClass("topfix");
            }else{
                $(".topbar").removeClass("topfix"); }
        });
    });
</script>


</html>
