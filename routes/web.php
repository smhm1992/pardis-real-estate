<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Ticket;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;



Route::get('/logout' , 'AdminController@logout')->name('logout');
Route::middleware(['auth:web'])->group(function () {
    Route::as('panel.')->prefix('/panel')->group(function () {

        Route::get('/', 'AdminController@index');
        Route::post('/edit_profile/{user}' ,'AdminController@edit_profile');
        Route::post('/change_pass/{user}' ,'AdminController@change_pass');
        Route::get('/home', 'AdminController@index')->name('home');

        Route::as('category.')->prefix('/category')->group(function () {
            Route::resource('/', 'CategoryController');
            Route::get('/index', 'CategoryController@index')->name('cat_index');
            Route::get('/create', 'CategoryController@create')->name('cat_create');
            Route::post('/edit/{category}', 'CategoryController@edit');
            Route::post('/store/{category}', 'CategoryController@store');
            Route::get('/delete/{category}', 'CategoryController@destroy');

        });

        Route::as('city.')->prefix('/city')->group(function () {
            Route::resource('/', 'CityController');
            Route::get('/index', 'CityController@index')->name('city_index');
            Route::get('/create', 'CityController@create')->name('city_create');
            Route::post('/edit/{city}', 'CityController@edit');
            Route::post('/store/{city}', 'CityController@store');
            Route::get('/delete/{city}', 'CityController@destroy');

        });

        Route::as('location.')->prefix('/location')->group(function () {
            Route::resource('/', 'LocationController');
            Route::get('/index', 'LocationController@index')->name('loc_index');
            Route::get('/create', 'LocationController@create')->name('loc_create');
            Route::post('/edit/{location}', 'LocationController@edit');
            Route::post('/store/{location}', 'LocationController@store');
            Route::get('/delete/{location}', 'LocationController@destroy');

        });

        Route::as('estate.')->prefix('/estate')->group(function () {
            Route::resource('/', 'EstateController');
            Route::get('/index', 'EstateController@index')->name('est_index');
            Route::get('/create', 'EstateController@create')->name('est_create');
            Route::get('/search', 'EstateController@search')->name('est_search');
            Route::post('/edit/{estate}', 'EstateController@edit');
            Route::post('/store/{estate}', 'EstateController@store');
            Route::get('/delete/{estate}', 'EstateController@destroy');
        });


        Route::as('house.')->prefix('/house')->group(function () {
            Route::resource('/', 'HouseController');
            Route::get('/house', 'HouseController@index')->name('est_index');
            Route::post('/edit/{estate}', 'HouseController@edit');
            Route::get('/delete/{estate}', 'HouseController@destroy');
        });


        Route::as('apartment.')->prefix('/apartment')->group(function () {
            Route::resource('/', 'ApartmentController');
            Route::get('/apartment', 'ApartmentController@index')->name('est_index');
            Route::post('/edit/{estate}', 'ApartmentController@edit');
            Route::get('/delete/{estate}', 'ApartmentController@destroy');
        });


        Route::as('agriculture.')->prefix('/agriculture')->group(function () {
            Route::resource('/', 'AgricultureController');
            Route::get('/agriculture', 'AgricultureController@index')->name('est_index');
            Route::post('/edit/{estate}', 'AgricultureController@edit');
            Route::get('/delete/{estate}', 'AgricultureController@destroy');
        });


        Route::as('earth.')->prefix('/earth')->group(function () {
            Route::resource('/', 'EarthController');
            Route::get('/earth', 'EarthController@index')->name('est_index');
            Route::post('/edit/{estate}', 'EarthController@edit');
            Route::get('/delete/{estate}', 'EarthController@destroy');
        });


        Route::as('commercial.')->prefix('/commercial')->group(function () {
            Route::resource('/', 'CommercialController');
            Route::get('/commercial', 'CommercialController@index')->name('est_index');
            Route::post('/edit/{estate}', 'CommercialController@edit');
            Route::get('/delete/{estate}', 'CommercialController@destroy');
        });

        Route::as('order.')->prefix('/order')->group(function () {
            Route::get('/', 'OrderController@index')->name('index');
            Route::post('/store', 'OrderController@store');
            Route::post('/edit/{order}', 'OrderController@edit');
            Route::post('/delete_order/{order}', 'OrderController@delete_order');
        });

        Route::as('ladder.')->prefix('/ladder')->group(function () {
            Route::get('/', 'LadderController@index')->name('index');
            Route::post('/edit/{estate}', 'LadderController@edit');
        });


        Route::as('ticket.')->prefix('/ticket')->group(function () {
            Route::resource('/', 'TicketController');
            Route::get('/p_index', 'TicketController@panel_index')->name('panel_index');
            Route::post('/send_reply/{ticket}', 'TicketController@send_reply')->name('send_reply');
            Route::get('/delete_ticket/{ticket}', 'TicketController@delete_ticket')->name('delete_ticket');

        });



        Route::as('notice.')->prefix('/notice')->group(function () {
            Route::resource('/', 'NoticeController');
            Route::post('/store', 'NoticeController@store')->name('store_not');
        });

    });

});

Route:: get('/', 'HomeController@index');
Route:: post('/estates/order', 'OrderController@store')->name('order.store');
Route:: get('/estates', 'EstateController@indexView');
Route:: get('/house', 'HouseController@indexView');
Route:: get('/apartment', 'ApartmentController@indexView');
Route:: get('/agriculture', 'AgricultureController@indexView');
Route:: get('/commercial', 'CommercialController@indexView');
Route:: get('/earth', 'EarthController@indexView');
Route:: post('/contactus/ticket', 'TicketController@store')->name('ticket.store');
Route:: get('/estates/{estate}', 'EstateController@singleView');
Route:: get('/contactus', 'ContactusController@index');
Route:: get('/aboutus', 'AboutusController@index');




Auth::routes();
