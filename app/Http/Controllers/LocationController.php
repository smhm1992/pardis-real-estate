<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use http\Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::paginate(10);
        $i = 1;
        return view('panel.locations.index', compact('locations', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.locations.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Location::create([
            'name' => $request->name,
        ]);

        Session::flash('message', "منطقه شما ثبت شد");
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Location $location)
    {
        $request->validate([
            'name' => 'required',
        ]);
        try {
            $location->name = $request->name;
            $location->save();
        } catch (Exception $exception) {
            Session::flash('message', $exception->getMessage());
            return Redirect::back();
        }
        Session::flash('message', "ویرایش با موفقیت انجام شد");
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();

        Session::flash('message', "حذف منطقه با موفقیت انجام شد");
        return Redirect::back();
    }
}
