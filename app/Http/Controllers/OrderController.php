<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders=Order::latest()->paginate(10);
        $i=1;
        return view('panel.orders.index' , compact('orders' , 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Order::create([
            'orderStatus' => 0 ,
            'FName' => $request->FName,
            'LName' => $request->LName,
            'phone' => $request->phone,
            'description' => $request->description,
            'estCode' => $request->estCode,
        ]);

        Session::flash('message', "درخواست شما ثبت شد. همکاران ما در اسرع وقت با شما تماس خواهند گرفت");
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order , Request $request)
    {

        $rules = [
            'orderStatus' =>  'required',
        ];
        $customMessages = [
            'orderStatus.required' => 'فیلد وضعیت اجباری است',
        ];
        $this->validate($request, $rules, $customMessages);


        try {
            $order->orderStatus = $request->orderStatus;

            $order->save();
        } catch (Exception $exception) {
            Session::flash('message', "خطایی رخ داده است");
            return Redirect::back();
        }
        Session::flash('message', "تغییر با موفقیت انجام شد");
        return Redirect::back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_order(Order $order)
    {
        $order->delete();

        Session::flash('message', "حذف مورد با موفقیت انجام شد");
        return Redirect::back();


    }
}
