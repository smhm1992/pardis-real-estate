<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use Symfony\Component\Routing\Tests\Fixtures\AnnotationFixtures\RequirementsWithoutPlaceholderNameController;

class AdminController extends Controller
{

    public function index()
    {

        $admin = User::first();
        return view('panel.dashboard', compact('admin'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/home');
    }

    public function change_pass(Request $request,  User $user)
    {
        $request->validate([]);
        if(Hash::make($request->old_pass) != $user->password){
            Session::flash('message', "پسورد قبلی خود را صحیح وارد کنید");
            return Redirect::back();
        }
        $user->password=Hash::make($request->new_pass);
        $user->save();

        Session::flash('message', "تغییر کلمه عبور با موفقیت انجام شد");
        return Redirect::back();


    }

    public function edit_profile(Request $request, User $user)
    {
        $request->validate([
//            'first_name' => 'required',
//            'last_name' => 'required',
            'email' => 'required',
//            'phone_number' => 'required',
        ]);

       try {
           $user->first_name = $request->first_name;
           $user->last_name = $request->last_name;
           $user->email = $request->email;
           $user->phone_number = $request->phone_number;
           $user->save();
       }catch (Exception $exception){
           Session::flash('message', $exception->getMessage());
           return Redirect::back();


       }
        Session::flash('message', "ویرایش با موفقیت انجام شد");
        return Redirect::back();
    }
}
