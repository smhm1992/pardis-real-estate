<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Estate;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estates = Estate::where('category_id', 1)->latest()->paginate(10);
        $cities = City::all();
        $locations = Location::all();
        $i = 1;
        return view('panel.houses.index', compact('estates','cities' ,'locations', 'i'));
    }

    public function indexView()
    {
        return view('house');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Estate $estate, Request $request)
    {

        $rules = [
            'code' =>  'required',
            'status' =>  'required',
            'deliverType' =>  'required',
//            'category_id' =>  'required',
            'area' =>  'required',
            'title' =>  'required',
            'description' =>  'required',
            'immediate' =>  'required',
//            'city_id' =>  'required',
//            'location_id' =>  'required',
            'type' => 'required',
//            'age' => 'required',
//            'elevator' => 'required',
//            'parking' => 'required',
//            'stockroom' => 'required',
//            'package' => 'required',
//            'rooms' => 'required',
//            'mortgage' => 'required',
//            'rent' => 'required',
//            'change' => 'required',
//            'price_per_meter' => 'required',
//            'price_all' => 'required'
        ];
        $customMessages = [
            'code.required' => 'فیلد کد ملک اجباری است',
            'status.required' => 'فیلد وضعیت اجباری است',
            'deliverType.required' => 'فیلد نوع تحویل اجباری است',
//            'category_id.required' => 'فیلد دسته بندی اجباری است',
            'area.required' => 'فیلد متراژ اجباری است',
            'title.required' => 'فیلد عنوان ملک اجباری است',
            'description.required' => 'فیلد توضیحات ملک اجباری است',
            'immediate' => 'فیلد فوری اجباری است',
//            'city_id.required' => 'فیلد شهر اجباری است',
//            'location_id.required' => 'فیلد منطقه اجباری است',
            'type.required' => 'فیلد نوع کاربری اجباری است',
            'image.required' => 'فیلد تصویر ملک اجباری است',
        ];
        $this->validate($request, $rules, $customMessages);


        try {
            $estate->code = $request->code;
            $estate->status = $request->status;
            $estate->deliverType = $request->deliverType;
//            $estate->category_id = $request->category_id;
            $estate->area = $request->area;
            $estate->title = $request->title;
            $estate->description = $request->description;
            $estate->immediate = $request->immediate;
//            $estate->city_id = $request->city_id;
//            $estate->location_id = $request->location_id;
            $estate->type = $request->type;
            $estate->isSell = $request->isSell;
            $estate->age = $request->age;
            $estate->parking = $request->parking;
            $estate->stockroom = $request->stockroom;
            $estate->package = $request->package;
            $estate->cabinet = $request->cabinet;
            $estate->terrace = $request->terrace;
            $estate->rooms = $request->rooms;
            $estate->elevator = $request->elevator;
            $estate->document = $request->document;
            $estate->license = $request->license;
            $estate->water = $request->water;
            $estate->electricity = $request->electricity;
            $estate->gas = $request->gas;
            $estate->remote = $request->remote;
            $estate->waterWell = $request->waterWell;
            $estate->fence = $request->fence;
            $estate->mortgage = $request->mortgage;
            $estate->rent = $request->rent;
            $estate->change = $request->change;
            $estate->price_per_meter = $request->price_per_meter;
            $estate->price_all = $request->price_all;

//            if ($request->hasFile('image')) {
//                $image = $request->file('image');
//                $name = time() . '.' . $image->getClientOriginalName();
//                $destinationPath = public_path('/images');
//                $image->move($destinationPath, $name);
//                $estate->image = $name;
//            }
            $estate->save();
        } catch (Exception $exception) {
            Session::flash('message', "خطایی رخ داده است");
            return Redirect::back();
        }
        Session::flash('message', "ویرایش با موفقیت انجام شد");
        return Redirect::back();
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estate $estate)
    {
        $estate->delete();
        Session::flash('message', "حذف خانه/ویلا با موفقیت انجام شد");
        return Redirect::back();
    }
}
