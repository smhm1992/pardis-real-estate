<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\City;
use App\Models\Estate;
use App\Models\Image;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class EstateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estates = Estate::latest()->paginate(10);
        $cats = Category::all();
        $cities = City::all();
        $locations = Location::all();
        $i = 1;
        return view('panel.estates.index', compact('estates', 'cats','cities' ,'locations', 'i'));
    }



    public function indexView()
    {
        $estates = Estate::latest();
        return view('estates');
    }

    public function singleView()
    {
        return view('estate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Category::all();
        $cities = City::all();
        $locations = Location::all();
        return view('panel.estates.create', compact('cats','cities','locations'));
    }


    public function search(Request $request)
    {
        $estates = Estate::where('code', $request->code)->latest()->paginate(10);
        $cats = Category::all();
        $cities = City::all();
        $locations = Location::all();
        $i = 1;
        return view('panel.estates.search', compact('estates','cats','cities','locations','i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code' =>  'required',
            'status' =>  'required',
            'deliverType' =>  'required',
            'category_id' =>  'required',
            'area' =>  'required',
            'title' =>  'required',
            'description' =>  'required',
            'immediate' =>  'required',
            'city_id' =>  'required',
            'location_id' =>  'required',
            'type' => 'required',
//            'age' => 'required',
//            'elevator' => 'required',
//            'parking' => 'required',
//            'stockroom' => 'required',
//            'package' => 'required',
//            'rooms' => 'required',
//            'mortgage' => 'required',
//            'rent' => 'required',
//            'change' => 'required',
//            'price_per_meter' => 'required',
//            'price_all' => 'required'
        ];
        $customMessages = [
            'code.required' => 'فیلد کد ملک اجباری است',
            'status.required' => 'فیلد وضعیت اجباری است',
            'deliverType.required' => 'فیلد نوع تحویل اجباری است',
            'category_id.required' => 'فیلد دسته بندی اجباری است',
            'area.required' => 'فیلد متراژ اجباری است',
            'title.required' => 'فیلد عنوان ملک اجباری است',
            'description.required' => 'فیلد توضیحات ملک اجباری است',
            'immediate' => 'فیلد فوری اجباری است',
            'city_id.required' => 'فیلد شهر اجباری است',
            'location_id.required' => 'فیلد منطقه اجباری است',
            'type.required' => 'فیلد نوع کاربری اجباری است',
            'image.required' => 'فیلد تصویر ملک اجباری است',
        ];
        $this->validate($request, $rules, $customMessages);


        $estate = Estate::create([
            'code' => $request->code,
            'status' => $request->status,
            'deliverType' => $request->deliverType,
            'category_id' => $request->category_id,
            'area' => $request->area,
            'title' => $request->title,
            'description' => $request->description,
            'immediate' => $request->immediate,
            'city_id' => $request->city_id,
            'location_id' => $request->location_id,
            'type' => $request->type,
            'isSell' => $request->isSell,
            'age' => $request->age,
            'parking' => $request->parking,
            'stockroom' => $request->stockroom,
            'package' => $request->package,
            'cabinet' => $request->cabinet,
            'terrace' => $request->terrace,
            'rooms' => $request->rooms,
            'elevator' => $request->elevator,
            'document' => $request->document,
            'license' => $request->license,
            'water' => $request->water,
            'electricity' => $request->electricity,
            'gas' => $request->gas,
            'remote' => $request->remote,
            'waterWell' => $request->waterWell,
            'fence' => $request->fence,
            'mortgage' => $request->mortgage,
            'rent' => $request->rent,
            'change' => $request->change,
            'price_per_meter' => $request->price_per_meter,
            'price_all' => $request->price_all,
        ]);



//
//        if ($request->gallery_pic) {
//
//            try{
//                $main = $request->file('gallery_pic');
//            }catch (Exception $e){
//                $main = $request->file('gallery_pic');
//            }
//
//            $watermark = imagecreatefrompng('images/watermark.png');
//
//            imagealphablending($main, false);
//            imagesavealpha($main, true);
//
//            imagecopymerge($main, $watermark, 9, 9, 3, 6, 140, 111, 50);
//
//
//            imagedestroy($main);
//
//            imagedestroy($watermark);


            foreach ($request->gallery_pic as $gallery) {
                $banner = $gallery;
                $bannername = time() . '.' . $banner->getClientOriginalName();
                $destinationPath = public_path('/images');
                $banner->move($destinationPath, $bannername);
                Image::create([
                    'estate_id' => $estate->id,
                    'image' => $bannername,
                ]);
            }




        Session::flash('message', "ملک شما ثبت شد");
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Estate $estate, Request $request)
    {


        $rules = [
            'code' =>  'required',
            'status' =>  'required',
            'category_id' =>  'required',
            'area' =>  'required',
            'title' =>  'required',
            'description' =>  'required',
            'immediate' =>  'required',
            'city_id' =>  'required',
            'location_id' =>  'required',
            'type' => 'required',
//            'age' => 'required',
//            'elevator' => 'required',
//            'parking' => 'required',
//            'stockroom' => 'required',
//            'package' => 'required',
//            'rooms' => 'required',
//            'mortgage' => 'required',
//            'rent' => 'required',
//            'change' => 'required',
//            'price_per_meter' => 'required',
//            'price_all' => 'required'
        ];
        $customMessages = [
            'code.required' => 'فیلد کد ملک اجباری است',
            'status.required' => 'فیلد وضعیت اجباری است',
            'category_id.required' => 'فیلد دسته بندی اجباری است',
            'area.required' => 'فیلد متراژ اجباری است',
            'title.required' => 'فیلد عنوان ملک اجباری است',
            'description.required' => 'فیلد توضیحات ملک اجباری است',
            'immediate' => 'فیلد فوری اجباری است',
            'city_id.required' => 'فیلد شهر اجباری است',
            'location_id.required' => 'فیلد منطقه اجباری است',
            'type.required' => 'فیلد نوع کاربری اجباری است',
        ];
        $this->validate($request, $rules, $customMessages);


        try {
            $estate->code = $request->code;
            $estate->status = $request->status;
            $estate->category_id = $request->category_id;
            $estate->area = $request->area;
            $estate->title = $request->title;
            $estate->description = $request->description;
            $estate->immediate = $request->immediate;
            $estate->city_id = $request->city_id;
            $estate->location_id = $request->location_id;
            $estate->type = $request->type;
            $estate->age = $request->age;
            $estate->elevator = $request->elevator;
            $estate->parking = $request->parking;
            $estate->stockroom = $request->stockroom;
            $estate->package = $request->package;
            $estate->rooms = $request->rooms;
            $estate->mortgage = $request->mortgage;
            $estate->rent = $request->rent;
            $estate->change = $request->change;
            $estate->price_per_meter = $request->price_per_meter;
            $estate->price_all = $request->price_all;

//            if ($request->hasFile('image')) {
//                $image = $request->file('image');
//                $name = time() . '.' . $image->getClientOriginalName();
//                $destinationPath = public_path('/images');
//                $image->move($destinationPath, $name);
//                $estate->image = $name;
//            }
            $estate->save();
        } catch (Exception $exception) {
            Session::flash('message', "خطایی رخ داده است");
            return Redirect::back();
        }
        Session::flash('message', "ویرایش با موفقیت انجام شد");
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estate $estate)
    {
        $estate->delete();
        Session::flash('message', "حذف ملک با موفقیت انجام شد");
        return Redirect::back();
    }
}
