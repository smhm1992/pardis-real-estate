<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contactus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function panel_index()
    {
        $tickets=Ticket::latest()->paginate(10);
        $i=1;
        return view('panel.tickets.index' , compact('tickets' , 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'sender_name' => 'required',
            'sender_phone' => 'required',
            'sender_email' => 'required',
            'message' => 'required',
        ];
        $customMessages = [
            'sender_name.required' => 'فیلد نام و نام خانوادگی اجباری است',
            'sender_phone.required' => 'انتخاب تلفن اجباری است',
            'email.required' => 'فیلد ایمیل اجباری است',
            'message.required' => 'فیلد متن پیام اجباری است',
        ];
        $this->validate($request, $rules, $customMessages);

            Ticket::create([
                'sender_name' => $request->sender_name,
                'sender_phone' => $request->sender_phone,
                'sender_email' => $request->sender_email,
                'message' => $request->message,
            ]);

        Session::flash('message', "پیام شما ارسال شد");
        return Redirect::back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function send_reply(Ticket $ticket , Request $request)
    {

        $data = array('msg'=>$request->reply);
        Mail::send(['text'=>'mail'], $data, function($message) use ($ticket) {
            $message->to($ticket->sender_email, 'Vira gostaran Co. Title')->subject
            ('Laravel Basic Testing Mail');
            $message->from('amlak.pardis.zargari@gmail.com','Vira gostaran Co.');
        });

        $ticket->status=2;
        $ticket->save();


        Session::flash('message', "ایمیل با موفقیت ارسال شد");
        return Redirect::back();
    }


    public function delete_ticket(Ticket $ticket)
    {
        dd($ticket);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
