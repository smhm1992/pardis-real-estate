<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{

    protected $guarded=[];
    protected $table = 'estates';



    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function City()
    {
        return $this->belongsTo(City::class);
    }

    public function Location()
    {
        return $this->belongsTo(Location::class);
    }

    public function Image()
    {
        return $this->hasMany(Image::class);
    }

    public function Order()
    {
        return $this->hasMany(Order::class);
    }

}
