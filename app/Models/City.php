<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded=[];
    protected $table = 'cities';



    public function Estate()
    {
        return $this->hasMany(Estate::class);
    }
}
