<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $guarded=[];
    protected $table = 'locations';



    public function Estate()
    {
        return $this->hasMany(Estate::class);
    }
}
