<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded=[];
    protected $table = 'categories';


//    public function Children()
//    {
//        return $this->hasMany(Category::class, 'parent_id', 'id');
//    }
//    public function Parent()
//    {
//        return $this->belongsTo(Category::class, 'parent_id');
//    }

    public function Estate()
    {
        return $this->hasMany(Estate::class);
    }


}
