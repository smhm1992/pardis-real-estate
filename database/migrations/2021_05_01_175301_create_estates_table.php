<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('estates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->boolean('status')->default(1)->comment('1 -> active / 0 -> not active');
            $table->string('deliverType');
            $table->integer('category_id');
            $table->string('area');
            $table->string('title');
            $table->text('description');
            $table->boolean('immediate')->default(0)->comment('1 -> immediate / 0 -> not immediate');
            $table->integer('city_id');
            $table->integer('location_id');
            $table->string('type')->nullable();
            $table->boolean('isSell')->default(0)->comment('1 -> sell / 0 -> not')->nullable();
            $table->string('age')->nullable();
            $table->boolean('parking')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('stockroom')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('package')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('cabinet')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('terrace')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->string('rooms')->nullable();
            $table->boolean('elevator')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('document')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('license')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('water')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('electricity')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('gas')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('remote')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('waterWell')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->boolean('fence')->default(0)->comment('1 -> it has / 0 -> not')->nullable();
            $table->string('mortgage')->nullable();
            $table->string('rent')->nullable();
            $table->boolean('change')->default(0)->comment('1 -> yes / 0 -> no')->nullable();
            $table->double('price_per_meter')->nullable();
            $table->double('price_all')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
