<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sender_name');
            $table->string('sender_phone')->nullable();
            $table->string('sender_email');
            $table->string('subject');
            $table->text('message');
            $table->text('reply_message')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1->new ticket / 2->replyed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
